import UIKit
import CoreData

var ID: NSManagedObjectID?

class RegistrationScreenViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.password.endEditing(true)
        self.repeatPassword.endEditing(true)
        self.username.endEditing(true)
        return false
    }
    
    @IBAction func registerButtonPush(_ sender: Any) {
        
        if password.text == "" || repeatPassword.text == "" || username.text == "" {
            self.errorLabel.text = "Fill all the fields"
            return
        }
        
        if password.text!.count < 6 || repeatPassword.text!.count < 6{
            self.errorLabel.text = "Password must contain more than 5 symbols"
            return
        }
        
        if password.text != repeatPassword.text{
            let alert = UIAlertController(title: "Wrong password", message: "Password doesn't match", preferredStyle: .alert)
            let okAlert = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAlert)
            present(alert, animated: true, completion: nil)
            repeatPassword.text = nil
        }
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "User",
                                       in: managedContext)!
        
        let person = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        // 3
        person.setValue(self.username.text, forKeyPath: "username")
        person.setValue(self.password.text, forKeyPath: "password")
        
        // 4
        do {
            try managedContext.save()
            
            Model.shared.password = self.password.text!
            Model.shared.username = self.username.text!
            
            ID = person.objectID
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        let nc = storyboard?.instantiateViewController(withIdentifier: "mainScreenController")
        navigationController?.pushViewController(nc!, animated: true)
        
    }
}
    


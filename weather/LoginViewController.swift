import Foundation
import UIKit
import CoreData

class ViewController: UIViewController, UISearchBarDelegate, UITextFieldDelegate {

    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var registration: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var labelOne: UILabel!
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.systemFont(ofSize: 14),
        .foregroundColor: UIColor.black,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributeString = NSMutableAttributedString(string: "Registration ", attributes: yourAttributes)
        registration.setAttributedTitle(attributeString, for: .normal)

        
    }

    //переход между экранами
    @IBAction func pushButton(_ sender: Any) {
        
        if username.text == "" || password.text == "" {
            let alert = UIAlertController(title: "Empty fields", message: "Fill all the fields", preferredStyle: .alert)
            let okAlert = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAlert)
            present(alert, animated: true, completion: nil)
        }
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let req = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        req.predicate = NSPredicate(format: "(username == %@) AND (password == %@)", self.username.text ?? "", self.password.text ?? "")
        
        
        
        do {
            let res = try managedContext.fetch(req) as! [User]
            if (res.indices.contains(0)) {
                let user = res[0]
                ID = user.objectID
                
                Model.shared.password = self.password.text!
                Model.shared.username = self.username.text!
                
                let nc = storyboard?.instantiateViewController(withIdentifier: "mainScreenController")
                navigationController?.pushViewController(nc!, animated: true)
//
            } else {
                let alert = UIAlertController(title: "Wrong password", message: "Repeat the password", preferredStyle: .alert)
                let okAlert = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAlert)
                present(alert, animated: true, completion: nil)
            }
        } catch {
            print(" fetch err: ", error)
        }
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.password.endEditing(true)
        self.username.endEditing(true)
        return false
    }
}

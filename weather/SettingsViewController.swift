import UIKit
import CoreData

class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(logOut))

    }
    
    @objc func logOut() {
//        let nc = storyboard?.instantiateViewController(withIdentifier: "LogIn") as! UINavigationController
//        self.present(nc, animated: true, completion: nil)
        navigationController?.popToRootViewController(animated: true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.password.endEditing(true)
        self.repeatPassword.endEditing(true)
        return false
    }
    
    @IBAction func submitButtonPush(_ sender: Any) {
        
        if password.text!.count < 6 || repeatPassword.text!.count < 6 {
            errorLabel.text = "Password must contain more than 5 symbols"
            return
        }
        
        if password.text != repeatPassword.text {
            errorLabel.text = "Password doesn't math"
            return
        }
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entity
        let predicate = NSPredicate(format: "(username == %@) AND (password == %@)", Model.shared.username, Model.shared.password)
        request.predicate = predicate
        do {
            var results =
                try managedContext.fetch(request)
            let objectUpdate = results[0] as! NSManagedObject
            objectUpdate.setValue(password.text!, forKey: "password")
            do {
                try managedContext.save()
                
            }catch let error as NSError {
                print("Error in updating data. Error: \(error)")
            }
        }
        catch let error as NSError {
            print("Error in updating data. Error: \(error)")
        }
        
        navigationController?.popToRootViewController(animated: true)
        
    }
    
}

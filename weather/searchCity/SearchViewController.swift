//
//  SearchViewController.swift
//  weather
//
//  Created by Анастасия Ковалева on 11/9/19.
//  Copyright © 2019 Анастасия Ковалева. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate {
    
    
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.showsCancelButton = true
 
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "eu1.locationiq.com"
        urlComponents.path = "/v1/search.php"
        urlComponents.queryItems = [
            URLQueryItem(name: "key", value: "eef7f7fc3a6659"),
            URLQueryItem(name: "q", value: searchText),
            URLQueryItem(name: "format", value: "json")
        ]
        
        if (urlComponents.url == nil) {
            return
        }
        
        Model.shared.url = URL(string: urlComponents.url!.absoluteString)
        Model.shared.requestName()        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        searchBar.resignFirstResponder()
    }


    
}


import UIKit

class SearchCityTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        searchBar.delegate = self
        tableView.dataSource = self
        self.searchBar.showsCancelButton = true
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name("dataRefreshed"), object: nil, queue: nil) { (notification) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        Model.shared.cityArr.removeAll()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "eu1.locationiq.com"
        urlComponents.path = "/v1/search.php"
        urlComponents.queryItems = [
            URLQueryItem(name: "key", value: "e018ef5b0d692a"),
            URLQueryItem(name: "q", value: searchText),
            URLQueryItem(name: "format", value: "json")
        ]
        
        if (urlComponents.url == nil) {
            return
        }
        
        Model.shared.url = URL(string: urlComponents.url!.absoluteString)
        Model.shared.requestName()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        searchBar.resignFirstResponder()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model.shared.cityArr.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath)
        cell.textLabel?.font = UIFont.init(name: "Helvetica", size: 28)
        
        cell.textLabel?.text = Model.shared.cityArr[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Model.shared.lat = Model.shared.latArr[indexPath.row]
        Model.shared.lon = Model.shared.lonArr[indexPath.row]
        let constituents = Model.shared.cityArr[indexPath.row].components(separatedBy: ",")
        var s = ""
        if (constituents.indices.contains(0)) {
            s.append(constituents[0])
        }
        if (constituents.indices.contains(1)) {
            s.append(",")
            s.append(constituents[1])
        }
        Model.shared.currentCity = s
        
        Model.shared.addable = true
        
        let nc = storyboard?.instantiateViewController(withIdentifier: "currentWeather")
        navigationController?.pushViewController(viewController: nc!, animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name("currentWeatherRefreshed"), object: self)
        })
        
    }
}

extension UINavigationController {
    
    public func pushViewController(viewController: UIViewController,
                                   animated: Bool,
                                   completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
}



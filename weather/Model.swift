import UIKit
import CoreLocation

struct ResponseCity: Decodable {
    let display_name: String
    let lat: String
    let lon: String
}

struct ResponseWeather: Decodable {
    struct Currently: Decodable {
        let temperature: Double
        let humidity: Double
        let windSpeed: Double
    }
    struct Hourly: Decodable {
        let summary: String
    }
    struct DailyData: Decodable {
        let time: Double
        let temperatureLow: Double
        let temperatureHigh: Double
    }
    struct Daily: Decodable {
        let data: [DailyData]
    }
    let currently: Currently
    let hourly: Hourly
    let daily: Daily
}

class Model: NSObject {
    static let shared = Model()
    let name = ""
    
    var url = URL(string: "")
    var cityArr = [String]()
    var latArr = [String]()
    var lonArr = [String]()
    var firstTableArray = [City]()
    var lat = ""
    var lon = ""
    var username = ""
    var password = ""
    var addable = false
    var searchQuery = ""

    var currentCity = ""
    var currentWeatherData: ResponseWeather?
    
    func requestWeather(completion: @escaping () -> ()){
        
        let url = URL(string: "https://api.darksky.net/forecast/09e9508dd318e33744dbd6a13ae8eab0/\(self.lat),\(self.lon)?units=si")
        print("*****")
        let task = URLSession.shared.dataTask(with: url!) { (data, responce, error) in
            if let data = data {
                do {
                    let res = try JSONDecoder().decode(ResponseWeather.self, from: data)
                    print(res)
                    DispatchQueue.main.async {
                        self.currentWeatherData = res
                        completion()
                    }
                    
                } catch {
                    print("model1", error)
                    
                }
            }
        }
        task.resume()
    }
    
    
    func requestName() {
        print("tt")
        let task = URLSession.shared.dataTask(with: url!) { (data, responce, error) in
//            print(String(data: data!, encoding: String.Encoding.utf8))
            if let data = data {
                do {
                    let res = try JSONDecoder().decode([ResponseCity].self, from: data)
                    DispatchQueue.main.async {
                        
                        self.cityArr.removeAll()
                        self.latArr.removeAll()
                        self.lonArr.removeAll()
                        
                        for i in 0...4{
                            if (res.indices.contains(i)) {
                                
                                self.cityArr.append(res[i].display_name)
                                self.latArr.append(res[i].lat)
                                self.lonArr.append(res[i].lon)
                            }
                        }
                        NotificationCenter.default.post(name: NSNotification.Name("dataRefreshed"), object: self)
                    }
                } catch {
                    print("model2", error)
                }
            }
            
        }
        task.resume()

    }
}

import UIKit
import CoreData

@IBDesignable
class CurrentWeatherViewController: UIViewController {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var addCity: UIBarButtonItem!
    @IBOutlet weak var dayTableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Model.shared.addable)
        print("addable")
        if !Model.shared.addable {
            self.addCity.isEnabled = false
            self.addCity.tintColor = .clear
            navigationItem.rightBarButtonItems?.insert(UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.trash, target: self, action: #selector(deleteCityHandler)), at: 0)
        }
        
        title = Model.shared.username
        cityNameLabel.text = Model.shared.currentCity
        
        dayTableView.rowHeight = UITableView.automaticDimension
        dayTableView.estimatedRowHeight = 300

        NotificationCenter.default.addObserver(forName: NSNotification.Name("currentWeatherRefreshed"), object: nil, queue: nil) { (notification) in
            DispatchQueue.main.async {
                print("^^^^^^^")
                Model.shared.requestWeather(completion: {
                    if let data = Model.shared.currentWeatherData {
                        self.tempLabel.text = "\(Int(data.currently.temperature))°C"
                        self.humidity.text = "\(Int(data.currently.humidity * 100))%"
                        self.windSpeed.text = "\(data.currently.windSpeed) m/s"
                        self.summary.text = "\(data.hourly.summary)"
                        self.dayTableView.reloadData()
                        let oldFrame = self.dayTableView.frame
                        self.dayTableView.frame = CGRect(
                            x: oldFrame.minX,
                            y: oldFrame.minY,
                            width: oldFrame.width,
                            height: CGFloat(data.daily.data.count * 44)
                        )
                        let contentRect: CGRect = self.scrollView.subviews.reduce(into: .zero) { rect, view in
                            rect = rect.union(view.frame)
                        }
                        self.scrollView.contentSize = contentRect.size
                    }
                })
            }
        }
    }
    
    @objc
    func deleteCityHandler() {
        let titleFont = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20.0)]
        let messageFont = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)]
        
        let titleAttrString = NSMutableAttributedString(string: "Are you sure?", attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: "You cannot reverse this action.", attributes: messageFont)
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (UIAlertAction) in
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let req = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
            req.predicate = NSPredicate(format: "name == %@", Model.shared.currentCity ?? "")
            
            do {
                let res = try managedContext.fetch(req) as! [City]
                if (!res.indices.contains(0)) {
                    let entity =
                        NSEntityDescription.entity(forEntityName: "City",
                                                   in: managedContext)!
                    
                    let city = NSManagedObject(entity: entity,
                                               insertInto: managedContext)
                    
                    city.setValue(Model.shared.lat, forKeyPath: "lat")
                    city.setValue(Model.shared.lon, forKeyPath: "lon")
                    city.setValue(Model.shared.currentCity, forKeyPath: "name")
                    
                    try managedContext.save()
                }
                
                let req1 = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                req1.predicate = NSPredicate(format: "username == %@", Model.shared.username)
                
                let res1 = try managedContext.fetch(req1) as! [User]
                
                
                if (res1.indices.contains(0)) {
                    let user = res1[0]
                    
                    let req2 = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
                    req2.predicate = NSPredicate(format: "name == %@", Model.shared.currentCity ?? "")
                    
                    let res2 = try managedContext.fetch(req2) as! [City]
                    
                    if (!res2.indices.contains(0)) { return }
                    
                    user.removeFromCity(res2[0])
                    
                    try managedContext.save()
                    
                    let nc = self.storyboard?.instantiateViewController(withIdentifier: "mainScreenController")
                    self.navigationController?.pushViewController(nc!, animated: true)
                }
                
            } catch {
                print(" fetch err: ", error)
            }
        }))
        alert.addAction(UIAlertAction(title: "Keep", style: .cancel))
        present(alert, animated: true)
    }
    
    @IBAction func pushAddCity(_ sender: Any) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        req.predicate = NSPredicate(format: "name == %@", Model.shared.currentCity ?? "")
        
        do {
            let res = try managedContext.fetch(req) as! [City]
            if (!res.indices.contains(0)) {
                let entity =
                    NSEntityDescription.entity(forEntityName: "City",
                                               in: managedContext)!
                
                let city = NSManagedObject(entity: entity,
                                           insertInto: managedContext)
                
                city.setValue(Model.shared.lat, forKeyPath: "lat")
                city.setValue(Model.shared.lon, forKeyPath: "lon")
                city.setValue(Model.shared.currentCity, forKeyPath: "name")
                
                try managedContext.save()
            }
            
            let req1 = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            req1.predicate = NSPredicate(format: "username == %@", Model.shared.username)
            
            let res1 = try managedContext.fetch(req1) as! [User]
            
            
            if (res1.indices.contains(0)) {
                let user = res1[0]
                
                let req2 = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
                req2.predicate = NSPredicate(format: "name == %@", Model.shared.currentCity ?? "")
                
                let res2 = try managedContext.fetch(req2) as! [City]

                if (!res2.indices.contains(0)) { return }
                
                user.addToCity(res2[0])
                
                try managedContext.save()
                
                let nc = storyboard?.instantiateViewController(withIdentifier: "mainScreenController")
                navigationController?.pushViewController(nc!, animated: true)
            }
            
        } catch {
            print(" fetch err: ", error)
        }
    }
}

extension NSDate {
    func dayOfTheWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self as Date)
    }
}
    
extension CurrentWeatherViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath) as? CurrentWeatherViewCell
        let data = Model.shared.currentWeatherData?.daily.data[indexPath.row]
        if let data = data {
            cell?.dayName.text = NSDate(timeIntervalSince1970: data.time).dayOfTheWeek()
            cell?.temperatureRange.text = "\(data.temperatureLow)°C to \(data.temperatureHigh)°C"
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model.shared.currentWeatherData?.daily.data.count ?? 0
    }
}

import UIKit

class CurrentWeatherViewCell: UITableViewCell {
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var temperatureRange: UILabel!
}

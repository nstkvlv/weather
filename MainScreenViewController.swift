import UIKit
import CoreData

class MainScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var usernameItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Model.shared.username

        self.navigationItem.hidesBackButton = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        Model.shared.firstTableArray.removeAll()
        Model.shared.addable = false
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        req.predicate = NSPredicate(format: "ANY user.username == %@", Model.shared.username)
        
        do {
            let res = try managedContext.fetch(req) as! [City]
            for i in res {
                Model.shared.firstTableArray.append(i)
            }
        } catch {
            print("table error", error)
        }
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model.shared.firstTableArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath)
        cell.textLabel?.font = UIFont.init(name: "Helvetica", size: 23)
        
        cell.textLabel?.text = Model.shared.firstTableArray[indexPath.row].name ?? "Error"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Model.shared.searchQuery != "" {
            if let name = Model.shared.firstTableArray[indexPath.row].name {
                if name.lowercased().contains(Model.shared.searchQuery) {
                    return 110
                }
            }
        } else {
            return 110
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Model.shared.lat = Model.shared.firstTableArray[indexPath.row].lat ?? "0"
        Model.shared.lon = Model.shared.firstTableArray[indexPath.row].lon ?? "0"
        Model.shared.currentCity = Model.shared.firstTableArray[indexPath.row].name ?? "Error"

        
        Model.shared.addable = false
        
        let nc = storyboard?.instantiateViewController(withIdentifier: "currentWeather")
        
        navigationController?.pushViewController(viewController: nc!, animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name("currentWeatherRefreshed"), object: self)
        })
        
        
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        Model.shared.searchQuery = searchText.lowercased()
        self.tableView.reloadData()
    }
    
    @IBAction func onSort(_ sender: Any) {
        
        Model.shared.firstTableArray.sort { $0.name!.lowercased() < $1.name!.lowercased() }
        self.tableView.reloadData()
    }
    
}
